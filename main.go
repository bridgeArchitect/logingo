package logingo

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/gotk3/gotk3/gtk"
	"log"
	"strconv"
)

/* constant rows for messages */
const (
	notNumber = "It is not number!" /* if it is not number */
	myDial    = "My Dialogue"       /* if user edits personal dialogue */
)

/* builder of all interface */
var (
	builder *gtk.Builder
)

/* current dialogue */
var (
	curDial = data.MyDial
)

/* objects of main window */
var (
	mainWindow            *gtk.Window /* main window */
	logEntry, passEntry   *gtk.Entry  /* entry for login and password */
	signButton, logButton *gtk.Button /* button for registration and login */
	exitButton            *gtk.Button /* button to exit application */
	showLabel             *gtk.Label  /* label to show information */
)

/* objects of window for user */
var (
	userWindow            *gtk.Window     /* window of user */
	nameEntry             *gtk.Entry      /* entry to show name of user */
	renameButton          *gtk.Button     /* button to rename user */
	deleteButton          *gtk.Button     /* button to delete user */
	deleteMessButton      *gtk.Button     /* button to delete message of user*/
	renameEntry           *gtk.Entry      /* entry to enter new name */
	showUserLabel         *gtk.Label      /* label to show information for user */
	deleteShowLabel       *gtk.Label      /* label to show status of deleting */
	exitUserButton        *gtk.Button     /* button to exit window of user */
	makePostButton        *gtk.Button     /* button to make post */
	messageEntry          *gtk.Entry      /* entry to enter new message */
	deleteEntry           *gtk.Entry      /* entry to enter id of message to delete */
	showMessButton        *gtk.Button     /* button to show messages */
	showTextBuffer        *gtk.TextBuffer /* buffer for text of messages */
	createDialogueButton  *gtk.Button     /* button to create dialogue */
	errorDialogueLabel    *gtk.Label      /* label to show errors during working with dialogues */
	dialogueEntry         *gtk.Entry      /* entry to create dialogue */
	chooseDialogueButton  *gtk.Button     /* button to choose dialogue */
	chooseDialogueEntry   *gtk.Entry      /* entry to choose dialogue */
	informDialogueLabel   *gtk.Label      /* label to show current dialogue */
	myDialogueButton      *gtk.Button     /* button to put personal dialogue */
	showAllMessagesButton *gtk.Button     /* button to show all messages of user */
	oldPasswordEntry      *gtk.Entry      /* entry to enter old password to submit */
	newPasswordEntry      *gtk.Entry      /* entry to enter new password for changing */
	passwordButton        *gtk.Button     /* button to change password */
)

/* function to create window */
func createWindow(nameGlade, nameWindow string) *gtk.Window {

	var (
		err error /* variable for error */
	)

	/* adding of xml-file */
	err = builder.AddFromFile(nameGlade)
	handleError(err)

	/* creation of window */
	obj, err := builder.GetObject(nameWindow)
	handleError(err)
	win := obj.(*gtk.Window)

	/* return window */
	return win

}

/* function to create window */
func createWidget(nameWidget string) interface{} {

	/* creation and returning of widget */
	obj, err := builder.GetObject(nameWidget)
	handleError(err)
	return obj

}

/* function to rename user */
func renameUser() {

	var (
		oldLogin, newLogin string /* new and old login for renaming */
		err                error  /* variable for error */
		messRn             string /* received message during renaming */
	)

	/* receive new and old names */
	oldLogin, err = nameEntry.GetText()
	handleError(err)

	newLogin, err = renameEntry.GetText()
	handleError(err)

	/* make renaming */
	messRn = data.RenameUser(oldLogin, newLogin)
	/* handle result of renaming */
	if messRn == data.MessRnFail {
		showUserLabel.SetText(messRn)
	} else {
		showUserLabel.SetText("")
		nameEntry.SetText(newLogin)
	}

}

/* function to delete user */
func deleteUser() {

	var (
		login string /* login to delete message */
		err   error  /* variable for error */
	)

	/* receive name of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* delete record and close user window */
	data.DeleteUser(login)
	userWindow.Destroy()
	showLabel.SetText("Deleting is successful!")

}

/* function to make message */
func makeMessage() {

	var (
		login   string /* login to make message*/
		message string /* message to save in database */
		err     error  /* variable for error */
	)

	/* receive name of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* receive message of user */
	message, err = messageEntry.GetText()
	handleError(err)

	/* make post */
	data.MakePost(login, message, curDial)

}

/* function to show messages */
func showMessages() {

	var (
		login  string /* login to show messages */
		err    error  /* variable for error */
		answer string /* row of received messages */
	)

	/* receive name of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* receive and show answer */
	if curDial == data.MyDial {
		answer = data.ShowUserPosts(login, curDial)
		showTextBuffer.SetText(answer)
	} else {
		answer = data.ShowDialogue(curDial)
		showTextBuffer.SetText(answer)
	}

}

/* function to delete message */
func deleteMessage() {

	var (
		idMess int    /* id of message */
		login  string /* login of user */
		err    error  /* variable for error */
		rowDl  string /* received row of id */
		messDl string /* received message of deleting */
	)

	/* default empty label for information label */
	deleteShowLabel.SetText("")

	/* receive id of message and check row */
	rowDl, err = deleteEntry.GetText()
	handleError(err)
	idMess, err = strconv.Atoi(rowDl)
	if err != nil {
		deleteShowLabel.SetText(notNumber)
	}

	/* receive login of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* delete message and check output */
	messDl = data.DeletePost(idMess, login)
	if messDl == data.MessDlFail {
		deleteShowLabel.SetText(messDl)
	}

}

/* function to choose dialogue */
func chooseDialogue() {

	var (
		name1, name2 string /* names of persons for dialogue */
		err          error  /* variable for error */
		messDial     string /* messages during creation */
		id           int    /* id of dialogue */
	)

	/* default empty label for label to show errors */
	errorDialogueLabel.SetText("")

	/* get the first name */
	name1, err = nameEntry.GetText()
	handleError(err)

	/* get the second name */
	name2, err = chooseDialogueEntry.GetText()
	handleError(err)

	/* get id of this dialogue and handle error */
	id, messDial = data.FindDialogue(name1, name2)
	if messDial == data.DialFdSuc {
		curDial = id
		informDialogueLabel.SetText(name2)
	} else {
		errorDialogueLabel.SetText(messDial)
	}

}

/* function to make dialogue */
func makeDialogue() {

	var (
		name1, name2 string /* names of persons for dialogue */
		err          error  /* variable for error */
		messDial     string /* messages during creation */
	)

	/* default empty label for label to show errors */
	errorDialogueLabel.SetText("")

	/* get the first name */
	name1, err = nameEntry.GetText()
	handleError(err)

	/* get the second name */
	name2, err = dialogueEntry.GetText()
	handleError(err)

	/* make dialogue */
	messDial = data.MakeDialogue(name1, name2)
	if messDial != data.DialMkSuc {
		errorDialogueLabel.SetText(messDial)
	}

}

/* function to put personal dialogue */
func putMyDialogue() {

	curDial = -1
	informDialogueLabel.SetText(myDial)

}

/* function to show all messages of user */
func showAllMessages() {

	var (
		login  string /* name of user */
		err    error  /* variable for error */
		answer string /* answer for user */
	)

	/* get name of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* receive and show answer */
	answer = data.ShowAllDialogues(login)
	showTextBuffer.SetText(answer)

}

func changePassword() {

	var (
		oldPassword, newPassword string /* new and old password */
		login                    string /* login of user */
		err                      error  /* variable for error */
		mess                     string /* message of function to change password */
	)

	/* receive old and new password */
	oldPassword, err = oldPasswordEntry.GetText()
	handleError(err)
	newPassword, err = newPasswordEntry.GetText()
	handleError(err)

	/* receive name of user */
	login, err = nameEntry.GetText()
	handleError(err)

	/* change password and check output message */
	mess = data.ChangePassword(login, oldPassword, newPassword)
	if mess == data.MessPsFail {
		showUserLabel.SetText(mess)
	}

}

/* function to create and show user window */
func createUserWindow(login string) {

	var (
		err error /* variable for error */
	)

	/* exit old window */
	if userWindow != nil {
		userWindow.Destroy()
	}

	/* set personal dialogue default */
	curDial = data.MyDial

	/* creation of window for user */
	userWindow = createWindow("userWindow.glade", "userWindow")
	userWindow.SetName("userWindow")
	handleError(err)

	/* show name of user */
	nameEntry = createWidget("nameEntry").(*gtk.Entry)
	nameEntry.SetText(login)

	/* receive label to show messages */
	showUserLabel = createWidget("showUserLabel").(*gtk.Label)

	/* receive label to show messages about deleting */
	deleteShowLabel = createWidget("deleteShowLabel").(*gtk.Label)

	/* receive label to show errors during working with dialogue */
	errorDialogueLabel = createWidget("errorDialogueLabel").(*gtk.Label)

	/* receive label to show current dialogues */
	informDialogueLabel = createWidget("informDialogueLabel").(*gtk.Label)

	/* receive entry to enter new name */
	renameEntry = createWidget("renameEntry").(*gtk.Entry)

	/* receive entry to enter new message */
	messageEntry = createWidget("messageEntry").(*gtk.Entry)

	/* receive entry to delete message of user */
	deleteEntry = createWidget("deleteEntry").(*gtk.Entry)

	/* receive entry to create dialogues */
	dialogueEntry = createWidget("dialogueEntry").(*gtk.Entry)

	/* receive entry to choose dialogues */
	chooseDialogueEntry = createWidget("chooseDialogueEntry").(*gtk.Entry)

	/* receive entry to submit password */
	oldPasswordEntry = createWidget("oldPasswordEntry").(*gtk.Entry)

	/* receive entry to set new password */
	newPasswordEntry = createWidget("newPasswordEntry").(*gtk.Entry)

	/* receive buffer to show messages */
	showTextBuffer = createWidget("showTextBuffer").(*gtk.TextBuffer)

	/* receive button to rename user */
	renameButton = createWidget("renameButton").(*gtk.Button)
	_, err = renameButton.Connect("clicked", renameUser)
	handleError(err)

	/* receive button to delete user */
	deleteButton = createWidget("deleteButton").(*gtk.Button)
	_, err = deleteButton.Connect("clicked", deleteUser)
	handleError(err)

	/* receive button to make post */
	makePostButton = createWidget("makePostButton").(*gtk.Button)
	_, err = makePostButton.Connect("clicked", makeMessage)
	handleError(err)

	/* receive button to show posts */
	showMessButton = createWidget("showMessButton").(*gtk.Button)
	_, err = showMessButton.Connect("clicked", showMessages)
	handleError(err)

	/* receive button to delete messages */
	deleteMessButton = createWidget("deleteMessButton").(*gtk.Button)
	_, err = deleteMessButton.Connect("clicked", deleteMessage)
	handleError(err)

	/* receive button to choose dialogue */
	chooseDialogueButton = createWidget("chooseDialogueButton").(*gtk.Button)
	_, err = chooseDialogueButton.Connect("clicked", chooseDialogue)
	handleError(err)

	/* receive button to create dialogue */
	createDialogueButton = createWidget("createDialogueButton").(*gtk.Button)
	_, err = createDialogueButton.Connect("clicked", makeDialogue)
	handleError(err)

	/* receive button to put personal dialogue */
	myDialogueButton = createWidget("myDialogueButton").(*gtk.Button)
	_, err = myDialogueButton.Connect("clicked", putMyDialogue)
	handleError(err)

	/* receive button to show all messages of user */
	showAllMessagesButton = createWidget("showAllMessagesButton").(*gtk.Button)
	_, err = showAllMessagesButton.Connect("clicked", showAllMessages)
	handleError(err)

	/* receive button to change password */
	passwordButton = createWidget("passwordButton").(*gtk.Button)
	_, err = passwordButton.Connect("clicked", changePassword)
	handleError(err)

	/* receive success to exit button */
	exitUserButton = createWidget("exitUserButton").(*gtk.Button)
	_, err = exitUserButton.Connect("clicked", func() {
		userWindow.Destroy()
	})
	handleError(err)

	/* showing of new window */
	userWindow.ShowAll()

}

/* function to handle error */
func handleError(err error) {

	/* if error exists */
	if err != nil {
		log.Fatal(err)
	}

}

/* main function */
func main() {

	var (
		err error /* variable for error */
	)

	/* initialization of GTK library */
	gtk.Init(nil)
	builder, err = gtk.BuilderNew()
	handleError(err)

	/* creation of main window */
	mainWindow = createWindow("mainWindow.glade", "mainWindow")
	mainWindow.SetName("mainWindow")
	_, err = mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})
	handleError(err)

	/* creation of widgets */
	logEntry = createWidget("logEntry").(*gtk.Entry)
	passEntry = createWidget("passEntry").(*gtk.Entry)
	signButton = createWidget("signButton").(*gtk.Button)
	logButton = createWidget("logButton").(*gtk.Button)
	exitButton = createWidget("exitButton").(*gtk.Button)
	showLabel = createWidget("showLabel").(*gtk.Label)

	/* function to make new record */
	_, err = signButton.Connect("clicked", func() {

		var (
			messMk          string /* message of result */
			login, password string /* login and password */
			errSign         error  /* error during creation of record */
		)

		/* get password and login */
		login, errSign = logEntry.GetText()
		handleError(errSign)
		password, errSign = passEntry.GetText()
		handleError(errSign)

		/* make record and handle error */
		messMk = data.MakeUser(login, password)
		if messMk != "" {
			showLabel.SetText(messMk)
		} else {
			showLabel.SetText("")
		}

	})
	handleError(err)

	/* function to login in the system */
	_, err = logButton.Connect("clicked", func() {

		var (
			messCh          string /* message of result */
			login, password string /* login and password */
			errLog          error  /* error during creation of record */
		)

		/* get password and login */
		login, errLog = logEntry.GetText()
		handleError(errLog)
		password, errLog = passEntry.GetText()
		handleError(errLog)

		/* check record and handle error */
		messCh = data.CheckUser(login, password)
		if messCh == data.MessChFail {

			showLabel.SetText(messCh)

		} else if messCh == data.MessChSuc {

			/* default message */
			showLabel.SetText("")
			/* create and show user window */
			createUserWindow(login)

		}

	})
	handleError(err)

	/* function to close window */
	_, err = exitButton.Connect("clicked", func() {
		mainWindow.Close()
	})
	handleError(err)

	/* show window and launch GTK library */
	mainWindow.ShowAll()
	gtk.Main()

}
