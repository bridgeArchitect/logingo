package logingo

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"strconv"
)

/* structure to save instance of user */
type user struct {
	Id       int    `sql:"type:int(11)"`      /* id of user */
	Login    string `sql:"type:varchar(100)"` /* login */
	Password int    `sql:"type:int(11)"`      /* password (hash code) */
}

/* structure to save instance of post */
type post struct {
	Id      int    `gorm:"primary_key:yes;AUTO_INCREMENT"` /* id of post */
	Message string `sql:"type:varchar(100)"`               /* message of post */
	Name    string `sql:"type:varchar(30)"`                /* login of user */
	IdDial  int    `gorm:"type:int(11);Column:idDial"`     /* id of dialogue */
}

/* structure to save instance of dialogue */
type dialogue struct {
	Id    int    `gorm:"primary_key:yes;AUTO_INCREMENT"` /* id of dialogue */
	Name1 string `sql:"type:varchar(30)"`                /* the first member of dialogue */
	Name2 string `sql:"type:varchar(30)"`                /* the second member of dialogue */
}

/* constant rows for messages */
const (
	MessChSuc    = "You are logged in."
	MessChFail   = "You are not logged in."
	MessMkSuc    = "Registration is successful!"
	MessMkFail   = "This user exists in system!"
	MessRnSuc    = "Renaming is successful!"
	MessRnFail   = "This user exists in system!"
	MessDlSuc    = "Deleting is successful."
	MessDlFail   = "This message does not exist for you!"
	MessPsSuc    = "Your password changes."
	MessPsFail   = "Password is incorrect."
	DialMkSuc    = "Creating of dialogue is successful."
	DialMkExFail = "This dialogue exists!"
	DialMkNmFail = "Name of person does not exist!"
	DialFdSuc    = "Dialogue exists."
	DialFdFail   = "Such dialogue does not exist."
)

/* constant rows to open database */
const (
	typeOrmSQL = "mysql"
	dataOrmSQL = "go:Moskow2012@/service?"
)

/* if the dialog does not exist for the post */
const (
	MyDial = -1
)

/* function to handle error */
func handleError(err error) {

	if err != nil {
		log.Fatal(err)
	}

}

/* function to get hash code */
func getHashCode(row string) int {

	var (
		i        int /* iterator */
		hashCode int /* hash code */
	)

	/* calculate hash code */
	i = 0
	hashCode = 0
	for i < len(row) {
		hashCode += (1255*int(row[i]) + 6173) % 29282
		i++
	}

	/* return hash code */
	return hashCode

}

/* function to open database using ORM */
func openDatabaseORM() *gorm.DB {

	var (
		db  *gorm.DB /* database */
		err error    /* variable for error */
	)

	/* creation of ORM-connection */
	db, err = gorm.Open(typeOrmSQL, dataOrmSQL)
	handleError(err)
	return db;

}

/* sort posts by dialogues and id */
func sortPostsDial(posts *[]post) {

	var (
		gap       [12]int /* gaps for Shell sort */
		i, i1, i2 int     /* iterators */
		swapPost  post    /* post for swapping */
	)

	/* setting of gaps */
	gap[0] = 1
	gap[1] = 3
	gap[2] = 7
	gap[3] = 21
	gap[4] = 48
	gap[5] = 112
	gap[6] = 336
	gap[7] = 861
	gap[8] = 1968
	gap[9] = 4592
	gap[10] = 13776
	gap[11] = 33936

	/* Shell sort using id of dialogues */
	i = 11
	for i >= 0 {
		i1 = gap[i]
		for i1 < len(*posts) {
			i2 = i1 - gap[i]
			for i2 > 0 {
				if (*posts)[i2].IdDial > (*posts)[i2+gap[i]].IdDial {
					/* swapping of elements */
					swapPost = (*posts)[i2]
					(*posts)[i2] = (*posts)[i2+gap[i]]
					(*posts)[i2+gap[i]] = swapPost
				}
				i2 -= gap[i]
			}
			i1++
		}
		i--
	}

	/* Shell sort using id of posts */
	i = 4
	for i >= 0 {
		i1 = gap[i]
		for i1 < len(*posts) {
			i2 = i1 - gap[i]
			for i2 > 0 {
				if (*posts)[i2].Id > (*posts)[i2+gap[i]].Id && (*posts)[i2].IdDial < (*posts)[i2+gap[i]].IdDial {
					/* swapping of elements */
					swapPost = (*posts)[i2]
					(*posts)[i2] = (*posts)[i2+gap[i]]
					(*posts)[i2+gap[i]] = swapPost
				}
				i2 -= gap[i]
			}
			i1++
		}
		i--
	}

}

/* function to receive names of people for some dialogue */
func findUsersDial(idDial int) (string, string) {

	var (
		dbOrm        *gorm.DB   /* database of app using ORM */
		dialogues    []dialogue /* array of dialogues */
		name1, name2 string     /* names of persons for given dialogue */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive dialogues with given id from database */
	dbOrm.Where("id = ?", idDial).Find(&dialogues)

	/* save dialogue with given id */
	if len(dialogues) != 0 {
		name1 = dialogues[0].Name1
		name2 = dialogues[0].Name2
	}

	/* return names */
	return name1, name2

}

/* check record to login in the system */
func CheckUser(login, password string) string {

	var (
		dbOrm       *gorm.DB  /* database of app using ORM */
		users       []user    /* array of users */
		hashCode    string    /* hash code of password as row */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive rows with given password and login from this database */
	users = make([]user, 0, 1000)
	hashCode = strconv.Itoa(getHashCode(password))
	dbOrm.Where("login = ? AND password = ?", login, hashCode).Find(&users)

	/* if user in database meets once, it will be good */
	if len(users) == 1 {
		return MessChSuc
	} else if len(users) == 0 {
		return MessChFail
	}

	/* send error about many records with similar parameters */
	handleError(fmt.Errorf("many records exist in this database in the system"))
	return ""

}

/* make new record in the system */
func MakeUser(login, password string) string {

	var (
	    dbOrm          *gorm.DB  /* database of app using ORM */
		err            error     /* variable for error */
		users          []user    /* array of users */
		newUser        user      /* new user in the database */
	)

	/* open database of app using ORM */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive rows with similar password and login from this database */
	users = make([]user, 0, 1000)
	dbOrm.Where("login = ?", login).Find(&users)

	/* check user with this parameters */
	if len(users) != 0 {
		return MessMkFail
	}

	/* receive rows with all users from this database */
	users = make([]user, 0, 1000)
	dbOrm.Find(&users)

	/* insert new user in the system */
	newUser = user{users[len(users) - 1].Id + 1, login, getHashCode(password)}
	err = dbOrm.Create(&newUser).Error
	handleError(err)

	/* return successful message */
	return MessMkSuc

}

/* function to rename record */
func RenameUser(oldLogin, newLogin string) string {

	var (
		dbOrm       *gorm.DB  /* database of app using ORM */
		users       []user    /* array of users */
		modelUser   user      /* user to show model for ORM */
		modelDial   dialogue  /* dialogue to show moder for ORM */
		modelPost   post      /* post to show post for ORM */
	)

	/* open database of app using ORM */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive all users from database with given login */
	users = make([]user, 0, 100)
	dbOrm.Where("login = ?", newLogin).Find(&users)

	/* check whether user with this login exists */
	if len(users) != 0 {
		return MessRnFail
	}

	/* rename user in the system (users, posts, dialogues) */
	dbOrm.Model(&modelUser).Where("login = ?", oldLogin).Update("login", newLogin)
	dbOrm.Model(&modelDial).Where("name1 = ?", oldLogin).Update("name1", newLogin)
	dbOrm.Model(&modelDial).Where("name2 = ?", oldLogin).Update("name2", newLogin)
	dbOrm.Model(&modelPost).Where("name = ?", oldLogin).Update("name", newLogin)

	/* return successful message */
	return MessRnSuc

}

/* function to change password */
func ChangePassword(login, oldPassword, newPassword string) string {


	var (
		dbOrm       *gorm.DB  /* database of app using ORM */
		users       []user    /* array of users */
		hashCode    string    /* hash code of password */
		modelUser   user      /* user to show model for ORM */
	)

	/* open database of app using ORM */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive hash code of old password */
	hashCode = strconv.Itoa(getHashCode(oldPassword))

	/* receive all users from database with given login and old password */
	users = make([]user, 0, 100)
	dbOrm.Where("login = ? AND password = ?", login, hashCode).Find(&users)

	/* check correctness of password */
	if len(users) == 0 {
		return MessPsFail
	}

	/* set new password and finish work */
	dbOrm.Model(&modelUser).Where("login = ?",login).Update("password", getHashCode(newPassword))
	return MessPsSuc

}

/* function to delete record */
func DeleteUser(name string) {

	var (
		dbOrm       *gorm.DB /* database of app using ORM */
		modelUser   user     /* user to show model for ORM */
		modelDial   dialogue /* dialogue to show moder for ORM */
		modelPost   post     /* post to show post for ORM */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* delete user and his posts with dialogues in the system */
	dbOrm.Where("login = ?", name).Delete(modelUser)
	dbOrm.Where("name1 = ? OR name2 = ?", name).Delete(modelDial)
	dbOrm.Where("name = ?", name).Delete(modelPost)

}

/* function to make post */
func MakePost(name string, message string, idDial int) {

	var (
		dbOrm       *gorm.DB /* database of app using ORM */
		newPost     post     /* post to make new record in database */
		err         error    /* variable for error */
	)

	/* open database of app using ORM */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* insert new message in the system */
	newPost = post{Message: message, Name: name, IdDial: idDial}
	err = dbOrm.Create(&newPost).Error
	handleError(err)

}

/* function to show posts of user */
func ShowUserPosts(name string, idDial int) string {

	var (
		dbOrm       *gorm.DB  /* database of app using ORM */
		posts       []post    /* array of posts */
		i           int       /* iterator */
		answer      string    /* row for answer */
	)

	/* open database of app using ORM */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive all posts of this user from database */
	posts = make([]post, 0, 1000)
	dbOrm.Where("name = ? AND idDial = ?", name, strconv.Itoa(idDial)).Find(&posts)


	/* handle result of request into one row */
	i = 0
	answer = fmt.Sprintf("%5s %25s %10s\n", "ID", "Message", "Name")
	for i < len(posts) {
		answer += fmt.Sprintf("%5d %25s %10s\n", posts[i].Id, posts[i].Message, posts[i].Name)
		i++
	}

	/* return answer */
	return answer

}

/* function to show dialogue of user */
func ShowDialogue(idDial int) string {

	var (
		dbOrm       *gorm.DB  /* database of app using ORM */
		posts       []post    /* array of posts */
		i           int       /* iterator */
		answer      string    /* row for answer */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive all posts of this dialogue from database */
	posts = make([]post, 0, 1000)
	dbOrm.Where("idDial = ?", strconv.Itoa(idDial)).Find(&posts)

	/* handle result of request into one row */
	i = 0
	answer = fmt.Sprintf("%5s %25s %10s\n", "ID", "Message", "Name")
	for i < len(posts) {
		answer += fmt.Sprintf("%5d %25s %10s\n", posts[i].Id, posts[i].Message, posts[i].Name)
		i++
	}

	/* return answer */
	return answer

}

/* function to show all dialogues of user */
func ShowAllDialogues(name string) string {

	var (
		dbOrm        *gorm.DB   /* database of app using ORM */
		posts        []post     /* array of posts */
		dialogues    []dialogue /* array of dialogues of given user */
		idArray      []int      /* array of id for dialogues of given user */
		i            int        /* iterator */
		answer       string     /* row for answer */
		name1, name2 string     /* names of users for one dialogue */
	)

	/* open database of app */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive array of id for dialogues of given user */
	dialogues = make([]dialogue, 0, 1000)
	dbOrm.Select("id").Where("name1 = ? OR name2 = ?", name, name).Find(&dialogues)

	/* get id of necessary dialogues for further work */
	idArray = make([]int, len(dialogues))
	i = 0
	for i < len(dialogues) {
		idArray[i] = dialogues[i].Id
		i++
	}

	/* receive all posts of this user from database */
	posts = make([]post, 0, 1000)
	dbOrm.Where("idDial in (?)", idArray).Find(&posts)

	/* sort posts by id of dialogue */
	sortPostsDial(&posts)

	/* handle result of request into one row */
	i = 0
	answer = ""
	for i < len(posts) {
		if i != 0 {
			/* write new dialogue */
			if posts[i].IdDial != posts[i-1].IdDial {
				name1, name2 = findUsersDial(posts[i].IdDial)
				answer += fmt.Sprintf("Dialogue between %s and %s\n", name1, name2)
				answer += fmt.Sprintf("%5s %25s %10s\n", "ID", "Message", "Name")
			}
		} else {
			/* write the first dialogue */
			name1, name2 = findUsersDial(posts[0].IdDial)
			answer += fmt.Sprintf("Dialogue between %s and %s\n", name1, name2)
			answer += fmt.Sprintf("%5s %25s %10s\n", "ID", "Message", "Name")
		}
		/* write post */
		answer += fmt.Sprintf("%5d %25s %10s\n", posts[i].Id, posts[i].Message, posts[i].Name)
		i++
	}

	/* return answer */
	return answer

}

/* function to delete post */
func DeletePost(idMess int, name string) string {

	var (
		dbOrm     *gorm.DB  /* database of app using ORM */
		posts     []post    /* array of posts to check */
		modelPost post      /* post to show model for ORM */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive all posts of this user with given id from database */
	dbOrm.Where("id = ? AND name = ?", strconv.Itoa(idMess), name).Find(&posts)
	/* check result of request */
	if len(posts) == 0 {
		return MessDlFail
	}

	/* delete of given message */
	dbOrm.Where("id = ? AND name = ?", strconv.Itoa(idMess), name).Delete(&modelPost)
	return MessDlSuc

}

/* function to make dialogue */
func MakeDialogue(name1 string, name2 string) string {

	var (
		dbOrm      *gorm.DB   /* database of app using ORM */
		dialogues  []dialogue /* array of dialogues to check */
		users      []user     /* array of users to check */
		newDial    dialogue   /* new dialogue for database */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive rows with similar name1 and name2 from this database */
	dbOrm.Where("(name1 = ? AND name2 = ?) OR (name1 = ? AND name2 = ?)", name1, name2, name2, name1).Find(&dialogues)
	/* check dialogues with this parameters */
	if len(dialogues) != 0 {
		return DialMkExFail
	}

	/* receive rows with name2 from this database */
	dbOrm.Where("login = ?", name2).Find(&users)
	/* check user with this parameters */
	if len(users) == 0 {
		return DialMkNmFail
	}

	/* creating of dialogue */
	newDial = dialogue{Name1:name1, Name2:name2}
	dbOrm.Create(&newDial)
	return DialMkSuc

}

/* function to find dialogue */
func FindDialogue(name1 string, name2 string) (int, string) {

	var (
		dbOrm     *gorm.DB   /* database of users */
		dialogues []dialogue /* array of dialogues */
	)

	/* open database for users */
	dbOrm = openDatabaseORM()
	defer dbOrm.Close()

	/* receive objects with similar name1 and name2 from this database */
	dbOrm.Where("(name1 = ? AND name2 = ?) OR (name1 = ? AND name2 = ?)", name1, name2, name2, name1).Find(&dialogues)
	if len(dialogues) == 0 {
		/* show error */
		return -1, DialFdFail
	}

	/* return id of dialogue */
	return dialogues[0].Id, DialFdSuc

}
